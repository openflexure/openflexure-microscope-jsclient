# OpenFlexure Microscope JS Client

# This project has moved!

The JS client has now been merged into the main OpenFlexure Microscope Server.

https://gitlab.com/openflexure/openflexure-microscope-server/-/tree/master/openflexure_microscope/api/static

Please raise any issues and merge requests in the `openflexure-microscope-server` project.